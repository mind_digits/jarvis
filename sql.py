# coding=utf-8
from telegram.ext import Updater, CommandHandler, CallbackQueryHandler, MessageHandler, Filters
from telegram import InlineKeyboardMarkup, InlineKeyboardButton
from threading import * #Timer, Thread
import telegram
import sqlite3
import logging
import datetime
from time import sleep
import random
import copy
import httplib2
import apiclient.discovery
from oauth2client.service_account import ServiceAccountCredentials
from sql import *
from classes import *
from settings import *

def timer_kill(tid):
    id1 = 0
    for ti in range(0, len(jarvis.timers) - 1):
        if jarvis.timers[ti].tid == tid:
            id1 = ti
            break
    if id1:
        jarvis.timers.pop(id1)

def set_smth(fld, val):
    print("Устанавливаю:", fld, val)
    try:
        conn = sqlite3.connect("mydb.ext")
        cursor = conn.cursor()
        cursor.execute("""UPDATE home SET value = ? WHERE field = ? """, (val, fld))
        conn.commit()
        #timer_kill(tid)
    except sqlite3.Error as e:
        print("Error: ", e.args[0])
    finally:
        if conn:
            conn.close()

def check_smth(fld):
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    sql = "SELECT value FROM home WHERE field = ?"
    cursor.execute(sql, (fld,))
    a = cursor.fetchall()
    if len(a) > 0:
        return(a[0][0])
    else:
        return('no value')


def addusertodb(id, grid):
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()

    if get_group_info(0, 2, grid) == "0": #если группа закрылась
        return -1

    sql = "SELECT * FROM users WHERE userid = ? and groupid = ?"
    cursor.execute(sql, (id,grid))
    a = cursor.fetchall()
    if len(a) == 0:
        print('Новенький, добавляю', id)
        cursor.execute("""INSERT INTO users (userid, groupid, name, state) VALUES (?,?,?,?)""", (id, grid, "new", "1"))
        conn.commit()
        sql = "SELECT userid FROM ids WHERE userid = ?"
        cursor.execute(sql, (id,))
        b = cursor.fetchall()
        if len(b) == 0:
            cursor.execute("""INSERT INTO ids (userid, activegroup) VALUES (?,?)""", (id, grid))
            conn.commit()
            return 2
        else:
            cursor.execute("""UPDATE ids SET activegroup = ? WHERE userid = ?""", (grid, id))
            conn.commit()
            return 1
    else:
        print('Есть такой юзер в этой группе', a[0][0])
        return 0


def set_group_name(uid, name):
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    while 1:
        newgid = random.randint(100000, 999999)
        sql = "SELECT groupid FROM groups WHERE groupid = ?"
        cursor.execute(sql, (newgid,))
        a = cursor.fetchall()
        if len(a) == 0:
            break
    cursor.execute("""INSERT INTO groups (groupid, name, open, sorted, adminid) VALUES (?,?,?,?,?)""",
                   (newgid, name, "1", "0", uid))
    conn.commit()
    print("Новая группа, ID %s, имя %s" % (newgid, name))
    cursor.execute("""INSERT INTO users (groupid, userid, name, state) VALUES (?,?,?,?)""", (newgid, uid, "new", "10"))
    conn.commit()
    sql = "SELECT userid FROM ids WHERE userid = ?"
    cursor.execute(sql, (uid,))
    b = cursor.fetchall()
    if len(b) == 0:
        cursor.execute("""INSERT INTO ids (userid, activegroup) VALUES (?,?)""", (uid, newgid))
    else:
        cursor.execute("""UPDATE ids SET activegroup = ? WHERE userid = ?""", (newgid, uid))
    conn.commit()

def set_user_name(id, name):
    ag = get_user_active_group(id)
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    print('Новое имя', name, 'для', id, 'в группе', get_group_name(ag))
    cursor.execute("""UPDATE users SET name  = (?) WHERE userid = (?) and groupid = (?)""", (name, id, ag))
    conn.commit()

def check_uniq_name(id, name):
    ag = get_user_active_group(id)
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    sql = "SELECT name FROM users WHERE name = ? and userid = ? and groupid = ?"
    print('Проверка уникальности', name, id, ag)
    cursor.execute(sql, (name, id, ag))
    a = cursor.fetchall()
    if len(a):
        print('Повтор имени')
        return 0
    else:
        print('Имя уникально')
        return 1

def get_user_state(id):
    ag = get_user_active_group(id)
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    sql = "SELECT state FROM users WHERE userid = ? and groupid = ?"
    cursor.execute(sql, (id, ag))
    a = cursor.fetchall()
    if len(a):
        #print('Юзерстейт', a[0][0])
        return a[0][0]
    else:
        pass
        #print('Не нашел такого юзера', id, ag)

def set_user_state(id, st):
    ag = get_user_active_group(id)
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    #print('Новый стейт', st, id, ag)
    cursor.execute("""UPDATE users SET state = (?) WHERE userid = (?) and groupid = (?)""", (st, id, ag))
    conn.commit()

def get_user_name(uid, gid=0):
    ag = get_user_active_group(uid)
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    sql = "SELECT name FROM users WHERE userid = ? and groupid = ?"
    if gid == 0:
        cursor.execute(sql, (uid, ag))
    else:
        cursor.execute(sql, (uid, gid))
    a = cursor.fetchall()
    if len(a):
        #print('Юзернейм', a[0][0])
        return a[0][0]
    else:
        pass
        #print('Не нашел такого юзера', id, ag)

def get_goodboy(uid, gid):
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    sql = "SELECT goodboy FROM users WHERE userid = ? and groupid = ?"
    cursor.execute(sql, (uid, gid))
    a = cursor.fetchall()
    if len(a):
        return a[0][0]
    else:
        pass
        #print('Нет гудбоя', uid, gid)

def get_user_active_group(id):
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    sql = "SELECT activegroup FROM ids WHERE userid = ?"
    cursor.execute(sql, (id,))
    a = cursor.fetchall()
    if len(a):
        #print('Активная группа', a[0][0])
        return a[0][0]
    else:
        #print('Не нашел такого юзера', id)
        return 0

def search_key(key):
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    sql = "SELECT groupid FROM groups WHERE groupid = ?"
    cursor.execute(sql, (key,))
    a = cursor.fetchall()
    if len(a):
        #print('Нашел группу', a[0][0])
        return a[0][0]
    else:
        #print('Не нашел такой группы', key)
        return 0

def get_group_name(grid):
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    sql = "SELECT name FROM groups WHERE groupid = ?"
    cursor.execute(sql, (grid,))
    a = cursor.fetchall()
    if len(a):
        #print('Имя группы', a[0][0])
        return a[0][0]
    else:
        pass
        #print('Не нашел такой группы', a[0][0])

def get_group_info(id, lalala, gid = 0):
    ag = get_user_active_group(id)
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    if lalala == 0:
        sql = "SELECT info FROM groups WHERE groupid = ?"
    if lalala == 1:
        sql = "SELECT time FROM groups WHERE groupid = ?"
    if lalala == 2:
        sql = "SELECT open FROM groups WHERE groupid = ?"
    if lalala == 3:
        sql = "SELECT sorted FROM groups WHERE groupid = ?"
    if gid == 0:
        cursor.execute(sql, (ag,))
    else:
        cursor.execute(sql, (gid,))
    a = cursor.fetchall()
    if len(a):
        #print('Инфо группы', a[0][0])
        if a[0][0] is None:
            return "-"
        else:
            return a[0][0]

def get_user_info(uid, lalala, gid):
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    if lalala == 0:
        sql = "SELECT info0 FROM users WHERE userid = ? and groupid = ?"
    if lalala == 1:
        sql = "SELECT info1 FROM users WHERE userid = ? and groupid = ?"
    cursor.execute(sql, (uid, gid))
    a = cursor.fetchall()
    if len(a):
        #print('Инфо человека:', a[0][0])
        if a[0][0] is None:
            return "-"
        else:
            return a[0][0]


def set_user_info(id, lalala, info):
    ag = get_user_active_group(id)
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    if lalala == 0:
        cursor.execute("""UPDATE users SET info0 = (?) WHERE userid = (?) and groupid = (?)""", (info, id, ag))
    if lalala == 1:
        cursor.execute("""UPDATE users SET info1 = (?) WHERE userid = (?) and groupid = (?)""", (info, id, ag))
    conn.commit()

def set_group_info(uid, lalala, info):
    gid = get_user_active_group(uid)
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    if lalala == 0:
        cursor.execute("""UPDATE groups SET info = (?) WHERE groupid = (?)""", (info, gid))
    if lalala == 1:
        cursor.execute("""UPDATE groups SET time = (?) WHERE groupid = (?)""", (info, gid))
    if lalala == 2:
        cursor.execute("""UPDATE groups SET key = (?) WHERE groupid = (?)""", (info, gid))
    if lalala == 3:
        cursor.execute("""UPDATE groups SET name = (?) WHERE groupid = (?)""", (info, gid))
    conn.commit()


def set_goodboy(uid, boyid, gid):
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    cursor.execute("""UPDATE users SET goodboy = (?) WHERE userid = (?) and groupid = (?)""", (boyid, uid, gid))
    conn.commit()

def set_group_sorted(gid, s):
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    cursor.execute("""UPDATE groups SET sorted = (?) WHERE groupid = (?)""", (s, gid))
    conn.commit()

def check_group_sorted(gid):
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    sql = "SELECT sorted FROM groups WHERE groupid = ?"
    cursor.execute(sql, (gid,))
    a = cursor.fetchall()
    if len(a):
        if a[0][0] == "0":
            return 0
        if a[0][0] == "1":
            return 1
    else:
        #print("Не нашел такой группы")
        return -1


def get_all_groups(id):
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    sql = "SELECT groupid FROM users WHERE userid = ?"
    cursor.execute(sql, (id,))
    a = cursor.fetchall()
    b = []
    if len(a) > 0:
        for i in a:
            b.append(i[0])
    else:
        pass
        #print("Не нашел ни одной группы")
    return b

def get_group_users(gid, ineedid=False):
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    if ineedid:
        sql = "SELECT userid FROM users WHERE groupid = ?"
    else:
        sql = "SELECT name FROM users WHERE groupid = ?"
    cursor.execute(sql, (gid,))
    a = cursor.fetchall()
    b = []
    if len(a) > 0:
        print("Показываем участников группы", get_group_name(gid))
        for i in a:
            b.append(i[0])
            #print(i[0])
    else:
        pass
        #print("Не нашел ни одного юзера")
    #print(b)
    return b

def check_admin(id):
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    sql = "SELECT groupid FROM groups WHERE adminid = ?"
    cursor.execute(sql, (id,))
    a = cursor.fetchall()
    b = []
    if len(a) > 0:
        for i in a:
            b.append(i[0])
    else:
        #print("Нигде не админ")
        pass
    #print(b)
    return b

def amiingroup(uid, gid):
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    sql = "SELECT userid FROM users WHERE userid = ? and groupid = ?"
    cursor.execute(sql, (uid, gid))
    a = cursor.fetchall()
    if len(a) > 0:
            #print("Юзер в группе")
            return 1
    else:
        #print("Юзер не в группе")
        return 0

def open_close_group(gid, op):
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    if op == "0":
        cursor.execute("""UPDATE groups SET open = (?) WHERE groupid = (?)""", ("0", gid))
    if op == "1":
        cursor.execute("""UPDATE groups SET open = (?) WHERE groupid = (?)""", ("1", gid))
    conn.commit()


def del_santas(gid):
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    cursor.execute("""UPDATE users SET goodboy = (?) WHERE groupid = (?)""", ("", gid))
    conn.commit()
    print("Отменено распределение Сант в группе", get_group_name(gid))


def del_group(gid):
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    cursor.execute("""DELETE FROM groups WHERE groupid = (?)""", (gid, ))
    conn.commit()
    cursor.execute("""DELETE FROM users WHERE groupid = (?)""", (gid,))
    conn.commit()
    cursor.execute("""UPDATE ids SET activegroup = (?) WHERE activegroup = (?)""", ("0", gid))
    conn.commit()
    print("Группа удалена %s" % gid)


def del_user_from_group(uid, gid):
    print("Юзер %s удалился из группы %s" % (get_user_name(uid), get_group_name(uid)))
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    cursor.execute("""DELETE FROM users WHERE userid = (?) and groupid = (?)""", (uid, gid))
    conn.commit()
    cursor.execute("""UPDATE ids SET activegroup = (?) WHERE userid = (?)""", ("0", uid))
    conn.commit()


def get_admin_users():
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    sql = "SELECT adminid FROM groups"
    cursor.execute(sql)
    a = cursor.fetchall()
    b = []
    if len(a) > 0:
        for i in a:
            b.append(i[0])
    return b


def get_all_users():
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    sql = "SELECT userid FROM ids"
    cursor.execute(sql)
    a = cursor.fetchall()
    b = []
    if len(a) > 0:
        for i in a:
            b.append(i[0])
    return b

def get_some_users():  #2 января
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    sql = "SELECT DISTINCT users.userid FROM users join groups ON users.groupid == groups.groupid WHERE groups.sorted = (?)"
    cursor.execute(sql, ("1",))
    a = cursor.fetchall()
    b = []
    if len(a) > 0:
        for i in a:
            b.append(i[0])
    return b
