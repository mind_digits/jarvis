# coding=utf-8
from telegram.ext import Updater, CommandHandler, CallbackQueryHandler, MessageHandler, Filters
from telegram import InlineKeyboardMarkup, InlineKeyboardButton
#from threading import * #Timer, Thread
import telegram
import sqlite3
import logging
import datetime
from time import sleep
import random
import copy
import httplib2
import apiclient.discovery
from oauth2client.service_account import ServiceAccountCredentials
import random
from sql import *
#from classes import *
from settings import *
from serv import *



def arrange(bot, gid):
    santalist = get_group_users(gid, ineedid=True)
    print("Запускаю сортировку для группы", gid)
    nogiftlist = santalist[:]
    for santa in santalist:
        while 1:
            goodboy = nogiftlist[random.randint(0, len(nogiftlist)-1)]
            if goodboy == santa:
                if len(nogiftlist) == 1:
                    print("Тупик, необходимо начать распределение сначала")
                    return -1
                else:
                    print("Нельзя дарить самому себе")
            else:
                break
        nogiftlist.remove(goodboy)
        set_goodboy(santa, goodboy, gid)
    for santa in santalist:
        goodboy = get_goodboy(santa, gid)
        sleep(0.1)
        text = "Хоу-хоу-хоу, *%s*!\nВолшебство произошло в группе *%s*.\n"\
               "Мои олени выбрали для тебя классного подопечного.\n"\
               "Это *%s*, правда здорово?\n"\
               "Однажды этот человек, сидя на моих коленях, шепнул мне на ушко, что хочет:\n_%s_\n"\
               "Но решение, конечно, за тобой.\n"\
               "Кстати, вот где он будет во время вручения подарочков:\n_%s_\n"\
               "Мои волшебные эльфы помогут вам связаться в случае необходимости\n\n"\
               "Классный бот? Если ты так тоже считаешь, жми /thanks" \
               % (get_user_name(santa, gid), get_group_name(gid), get_user_name(goodboy, gid),
                  get_user_info(goodboy, 0, gid), get_user_info(goodboy, 1, gid))
        bad_symbols = "#*_><+=[]{}!?"
        print("Отправляю подопечного Санте", santa, get_user_name(santa), goodboy, get_user_name(goodboy))
        if any(s in get_user_info(goodboy, 0, gid) for s in bad_symbols) or \
                any(s in get_user_info(goodboy, 1, gid) for s in bad_symbols):
            bot.send_message(chat_id=santa, text=text)
            print("Плохие символы в сообщении Санте, отправляю без маркдауна")
        else:
            bot.send_message(chat_id=santa, text=text, parse_mode=telegram.ParseMode.MARKDOWN)
        print("Отправил")
        sleep(0.4)
        bot.send_animation(chat_id=santa, animation="https://media.giphy.com/media/9J8R3SaGMOhrK058Am/giphy.mp4")
    print("Закончил сортировку для группы", gid)
    bot.send_message(chat_id="182116723", text="Отсортирована группа %s, %s участников" % (get_group_name(gid), len(santalist)))
    set_group_sorted(gid, "1")
    open_close_group(gid, "0")
    return 1


def gogogo(bot, update):
    ul = get_some_users()
    #ul = ['182116723', '165799973']
    i = 0
    for u in ul:
        text = "Тайный РобоСанта 🎅🏾 всегда рад устроить праздник для хороших мальчиков и девочек.\n" \
               "Если вы считаете, что Санта тоже был *хорошим мальчиком*, можете подкинуть ему пару угольков на нового " \
               "робо-оленя и гирлянду для серверной стойки.\n" \
               "Это можно сделать по одной из ссылок:" \
               "\nhttps://money.yandex.ru/to/41001753683166" \
               "\nhttps://www.tinkoff.ru/sl/2zLVSuJRSKI"
        sleep(0.1)
        bot.send_message(chat_id=u, text=text, parse_mode=telegram.ParseMode.MARKDOWN)
        sleep(0.1)
        bot.send_animation(chat_id=u, animation="https://media.giphy.com/media/s6ZyMUYOp4SZO/giphy.mp4")
        i = i + 1
    print("Сделал рассылку по %s юзерам" % i)
    bot.send_message(chat_id="182116723", text="Сделал рассылку по %s юзерам" % i, parse_mode=telegram.ParseMode.MARKDOWN)


def trista(bot, update, args):
    print("/300", update.message.chat_id)
    all_list = get_all_users()
    bot.send_message(chat_id=update.message.chat_id, text=len(all_list))


def spam_admin(bot, update, args):
    uid = update.message.from_user.id
    text = ''
    for i in args:
        text = text + i + " "
    text = text[0:-1]
    ulist = get_admin_users()
    for i in ulist:
        sleep(0.1)
        bot.send_message(chat_id=i, text=text, parse_mode=telegram.ParseMode.MARKDOWN)

def thanks(bot, update):
    print("/thanks", update.message.chat_id)
    text = "Тайный РобоСанта 🎅🏾 всегда рад устроить праздник для хороших мальчиков и девочек.\n" \
           "Если вы считаете, что Санта тоже был *хорошим мальчиком*, можете подкинуть ему пару угольков на нового " \
           "робо-оленя и гирлянду для серверной стойки.\n" \
           "Это можно сделать по одной из ссылок:" \
           "\nhttps://money.yandex.ru/to/41001753683166"\
           "\nhttps://www.tinkoff.ru/sl/2zLVSuJRSKI"
    bot.send_message(chat_id=update.message.from_user.id, text=text, parse_mode=telegram.ParseMode.MARKDOWN)
    bot.send_animation(chat_id=update.message.chat_id,
                       animation="https://media.giphy.com/media/9J8R3SaGMOhrK058Am/giphy.mp4")


def admin(bot, update):
    uid = update.message.from_user.id
    adm_grps(uid, bot, update=update)

def newgroup(bot, update, args):
    uid = update.message.from_user.id
    if len(args) > 0:
        name = ''
        for i in args:
            name = name + i + " "
        name = name[0:-1]
        bad_symbols = "#*_><+=[]{}!?"
        if any(s in name for s in bad_symbols):
            bot.send_message(chat_id=update.message.chat_id, text="Нельзя использовать символы *#~ и т.д.")
            return
        set_group_name(uid, name)
        bot.send_message(chat_id=update.message.chat_id, text="Новая группа *%s* создана. А теперь напиши мне "
                                                          "короткое описание вашей группы. Может быть, какие-то "
                                                          "правила или рекомендации, ориентировочный бюджет. Или анекдот про оленей.\n"
                                                          "Место и время не пиши, для этого будет отдельный шаг." % name,
                         parse_mode=telegram.ParseMode.MARKDOWN)
        bot.send_message(chat_id="182116723", text="Новая группа *%s*" % name, parse_mode=telegram.ParseMode.MARKDOWN)

    else:
        bot.send_message(chat_id=update.message.chat_id, text="Нужно указать имя будущей группы после команды.\n"
                                                              "Например: /newgroup Озорные снежинки")



def start(bot, update, args):
    uid = update.message.from_user.id
    text = "Привет, я повелитель Тайных Сант. Как насчет немного подарочков?"
    if len(args) > 0:
        grid = search_key(args[0])
        if grid:
            a = addusertodb(uid, grid)
            if (a >= 1) or ((a == 0) and (get_user_state(uid) == "1")):
                text = "Поздравляю с успешным входом!\n" \
                       "Ты начал регистрироваться в операции *Тайный Санта* среди прекрасной команды *%s*.\n" \
                       "Когда зарегистрируются все участники, я проведу волшебную жеребьёвку и тебе достанется " \
                       "подопечный, которому к оговоренной дате ты должен будешь подготовить классный подарок.\n" \
                       "А кто-то станет твоим личным Тайным Сантой и подготовит подарочек специально для тебя.\n\n" \
                       "Тут всё серьезно, поэтому, чтобы продолжить, напиши мне\n" \
                       "\"*Я буду хорошим Cантой*\"\nКопипастить нельзя, я узнаю!" % get_group_name(grid)
                all_list = get_all_users()
                if (len(all_list) % 100 == 0) and (a == 2):
                    bot.send_message(chat_id="182116723", text="Ура, новые юзеры! Теперь их *%s*" % len(all_list), parse_mode=telegram.ParseMode.MARKDOWN)
            elif a == 0:
                text = "Привет! Ты уже есть в этой группе, добро пожаловать назад."
            elif a == -1:
                text = "Регистрация в эту группу уже закрыта."
        else:
            text = 'Ключ не верифицирован'
    else:
        text = text + '\nЧтоб присоединиться к группе, нужно пройти по секретной ссылке.\n' \
                      'Или создать свою группу командой /newgroup.'
    custom_keyboard = [['Мои группы']]
    reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard, resize_keyboard=True)
    bot.send_message(chat_id=update.message.chat_id, text=text,
                     reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)

def adm_grps(uid, bot, update=0, query=0):
    glist = check_admin(uid)
    groups_keyboard = [[]]
    reply_markup = telegram.InlineKeyboardMarkup(groups_keyboard)
    j = 0
    if len(glist):
        text = "Ты админ в следующих группах:\n"
        for i in glist:
            text = text + "\n*" + get_group_name(i) + "*"
            groups_keyboard.append([])
            groups_keyboard[j].append(
                InlineKeyboardButton("%s" % (get_group_name(i)), callback_data='admgroup:%s' % i))
            j = j + 1
        text = text + "\n\nВыбери группу и я покажу тебе кнопки всевластия."
        if update:
            bot.send_message(chat_id=update.message.chat_id, text=text,
                             reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)
        if query:
            bot.edit_message_text(text=text, chat_id=query.message.chat_id, message_id=query.message.message_id,
                                  reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)


def sh_grps(uid, bot, update=0, query=0):
    glist = get_all_groups(uid)
    text = 'Вот список твоих групп:\n\n'
    groups_keyboard = [[]]
    reply_markup = telegram.InlineKeyboardMarkup(groups_keyboard)
    print("Юзер смотрит свои группы, их", len(glist))
    if len(glist) == 0:
        text = "Пока нет ни одной группы\n"
    else:
        for i in glist:
            text = text + "*" + get_group_name(i) + "*\n"
            groups_keyboard.append(
                [InlineKeyboardButton("%s" % get_group_name(i), callback_data='showgroup:%s' % i)])
    text = text + "\n\nДля создания новой группы с блэкджеком и эльфами введи команду /newgroup\n" \
                  "\nУправлть своими созданными группами можно через команду /adm\n"
    if update:
        bot.send_message(text=text, chat_id=update.message.chat_id, message_id=update.message.message_id,
                         reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)
    if query:
        bot.edit_message_text(text=text, chat_id=query.message.chat_id, message_id=query.message.message_id,
                         reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)


def react_inline(bot, update):
    query = update.callback_query
    print("QUERY:", query.data)
    uid = query.from_user['id']
    if 'deltimer' in query.data:
        tid = query.data.split(':')[1]
        print(tid)
        print(len(jarvis.timers))
        new_timers = []
        for i in range(0, len(jarvis.timers)-1):
            if jarvis.timers[i].tid != tid:
                new_timers.append(jarvis.timers[i])
            else: print("Этот не попадет в список: ", jarvis.timers[i].tid  )
        jarvis.timers = new_timers
        if len(new_timers):
            reply_markup = telegram.InlineKeyboardMarkup([[]])
            bot.edit_message_text(text="Таймер удален", chat_id=query.message.chat_id, message_id=query.message.message_id,
                                  reply_markup=reply_markup)

    if ('showgroup' in query.data):
        gid = query.data.split(':')[1]
        text = "*" + get_group_name(gid) + "\n\nОписание*\n" + get_group_info(0, 0, gid=gid)
        text = text + "\n\n*Место и время*\n" + get_group_info(0, 1, gid=gid)
        if get_group_info(0, 2, gid) == "0":
            text = text + "\n\nРегистрация закрыта"
        else:
            text = text + "\n\nРегистрация открыта"
        text = text + "\n\nСсылка для регистрации\nhttps://t.me/tayniysantabot?start=%s" % (gid)
        text = text + "\n\n*Участники*"
        ulist = get_group_users(gid)
        for i in ulist:
            text = text + "\n" + i
        if ("new" in ulist):
            text = text + "\n\nnew - пользователи, которые прошли по ссылке, но еще не написали Я буду хорошим Сантой " \
                          "и не выбрали себе имя. Можно потыкать их палочкой или удалить через админку /adm перед " \
                          "распределением."

        groups_keyboard = []

        if check_group_sorted(gid):
            goodboy = get_goodboy(uid, gid)
            text = text + "\n\nМои олени выбрали для тебя классного подопечного.\n"\
                          "Это *%s*, правда здорово?\n"\
                          "Однажды этот человек шепнул мне на ушко, что хочет:\n_%s_\n"\
                          "Но решение, конечно, за тобой.\n"\
                          "Кстати, вот где он будет в сроки вручения подарочков:\n_%s_\n"\
                          "Мои волшебные эльфы помогут вам связаться в случае необходимости\n\n"\
                          % (get_user_name(goodboy, gid), get_user_info(goodboy, 0, gid), get_user_info(goodboy, 1, gid))
            #groups_keyboard.append([InlineKeyboardButton("Секретная почта", callback_data='mail:%s' % gid)])


        if amiingroup(uid, gid):
            groups_keyboard.append([InlineKeyboardButton("Покинуть группу", callback_data='cancel:%s' % gid)])
        groups_keyboard.append([InlineKeyboardButton("Назад", callback_data='mygroups')])
        reply_markup = telegram.InlineKeyboardMarkup(groups_keyboard)
        bot.edit_message_text(text=text, chat_id=query.message.chat_id, message_id=query.message.message_id,
                              reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)


    if ('cancel' in query.data):
        gid = query.data.split(':')[1]
        groups_keyboard = [[]]
        if get_group_info(0, 3, gid) == "0":
            text = "Ты уверен, что хочешь покинуть группу *" + get_group_name(gid) + "*?\n" \
                   "Пока Тайных Сант не распределят, ты сможешь вернуться, заново пройдя по пригласительной ссылке."
            groups_keyboard.append([InlineKeyboardButton("Да, точно выйти", callback_data='confcanc:%s' % gid)])
        else:
            text = "Упс, выйти из распределенной группы уже нельзя. Можно будет выйти только если админ сперва отменит " \
                   "распределение Сант."
            groups_keyboard.append([InlineKeyboardButton("Назад", callback_data='mygroups')])
        reply_markup = telegram.InlineKeyboardMarkup(groups_keyboard)
        bot.edit_message_text(text=text, chat_id=query.message.chat_id, message_id=query.message.message_id,
                              reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)

    if ('confcanc' in query.data):
        gid = query.data.split(':')[1]
        groups_keyboard = [[]]
        del_user_from_group(uid, gid)
        text = "Ты покинул группу *" + get_group_name(gid) + "*"
        reply_markup = telegram.InlineKeyboardMarkup(groups_keyboard)
        bot.edit_message_text(text=text, chat_id=query.message.chat_id, message_id=query.message.message_id,
                              reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)


    if ('admgroup' in query.data):
        gid = query.data.split(':')[1]
        print("Показываю админку группы", get_group_name(gid))
        text = "*✳️Админка " + get_group_name(gid) + "✳️\n\nОписание*\n" + get_group_info(0, 0, gid=gid)
        text = text + "\n\n*Место и время*\n" + get_group_info(0, 1, gid=gid)
        groups_keyboard = [[]]
        groups_keyboard.append([InlineKeyboardButton("Назад", callback_data='admmain')])
        if get_group_info(0, 2, gid) == "0":
            text = text + "\n\nРегистрация закрыта"
            groups_keyboard.append([InlineKeyboardButton("Открыть регистрацию", callback_data='openclose:%s' % gid)])
        else:
            text = text + "\n\nРегистрация открыта"
            groups_keyboard.append([InlineKeyboardButton("Закрыть регистрацию", callback_data='openclose:%s' % gid)])
        if check_group_sorted(gid):
            text = text + "\nУже распределены Тайные Санты!"
            groups_keyboard.append([InlineKeyboardButton("Обнулить Тайных Сант", callback_data='del1santa:%s' % gid)])
        else:
            text = text + "\nТайные Санты еще не распределены"
            groups_keyboard.append([InlineKeyboardButton("Распределить подарочки!", callback_data='sort:%s' % gid)])
        groups_keyboard.append([InlineKeyboardButton("Удалить группу", callback_data='delgrp:%s' % gid)])
        text = text + "\n\nСсылка для регистрации\nhttps://t.me/tayniysantabot?start=%s" % (gid)
        text = text + "\n\n*Участники*"
        ulist = get_group_users(gid)
        for i in ulist:
            text = text + "\n" + i
        if len(ulist):
            groups_keyboard.append([InlineKeyboardButton("Удалить участника", callback_data='delmember:%s' % gid)])
        reply_markup = telegram.InlineKeyboardMarkup(groups_keyboard)
        bot.edit_message_text(text=text, chat_id=query.message.chat_id, message_id=query.message.message_id,
                              reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)
    if ('openclose' in query.data):
        gid = query.data.split(':')[1]
        if get_group_info(0, 2, gid=gid) == "0": #если группа закрыта - открываем
            open_close_group(gid, "1")
            text = "Ура, регистрация снова открыта!"
        else:
            open_close_group(gid, "0")
            text = "Кто не успел, я не виноват. Группа закрыта."
        groups_keyboard = [[InlineKeyboardButton("Назад", callback_data='admgroup:%s' % gid)]]
        reply_markup = telegram.InlineKeyboardMarkup(groups_keyboard)
        bot.edit_message_text(text=text, chat_id=query.message.chat_id, message_id=query.message.message_id,
                              reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)
    if ('sort' in query.data):
        gid = query.data.split(':')[1]
        text = "Ты уверен? Больше никого не ждем?\nПотом добавить опоздавших будет нельзя, только перераспределять заново."
        glist = get_group_users(gid)
        groups_keyboard = []
        if "new" in glist:
            text = text + "\n\nВнимание! Кто-то не закончил регистрацию, я обнаружил имя NEW в списке.\n" \
                          "Они не написали Я буду хорошим Сантой или не ввели имя.\n" \
                          "Или попроси друзей закончить регистрацию, или удали их из группы."
        else:
            groups_keyboard.append([InlineKeyboardButton("Да", callback_data='srtconf:%s' % gid)])
        groups_keyboard.append([InlineKeyboardButton("Назад", callback_data='admgroup:%s' % gid)])
        reply_markup = telegram.InlineKeyboardMarkup(groups_keyboard)
        bot.edit_message_text(text=text, chat_id=query.message.chat_id, message_id=query.message.message_id,
                              reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)
    if ('srtconf' in query.data):
        gid = query.data.split(':')[1]
        if get_group_info(0, 3, gid) == "0":
            if len(get_group_users(gid, ineedid=True)) < 2:
                bot.send_message(chat_id=query.message.chat_id, text="В группе должно быть больше одного человека. "
                                                                     "Пригласи друзей!")
                return 0
            text = "❄️❄️❄️❄️❄️\nДа случится волшебство!"
            while arrange(bot, gid) == -1:
                continue
            bot.edit_message_text(text=text, chat_id=query.message.chat_id, message_id=query.message.message_id,
                              parse_mode=telegram.ParseMode.MARKDOWN)
    if ('del1santa' in query.data):
        gid = query.data.split(':')[1]
        text = "Ты уверен? Все Тайны Санты отвяжутся от своих подопечных. Новое распределение будет совсем другим."
        groups_keyboard = [[InlineKeyboardButton("Да", callback_data='delsantaconf:%s' % gid)]]
        groups_keyboard.append([InlineKeyboardButton("Назад", callback_data='admgroup:%s' % gid)])
        reply_markup = telegram.InlineKeyboardMarkup(groups_keyboard)
        bot.edit_message_text(text=text, chat_id=query.message.chat_id, message_id=query.message.message_id,
                              reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)
    if ('delsantaconf' in query.data):
        gid = query.data.split(':')[1]
        text = "Ок, всё удалил."
        del_santas(gid)
        set_group_sorted(gid, "0")
        groups_keyboard = [[]]
        groups_keyboard.append([InlineKeyboardButton("Назад", callback_data='admgroup:%s' % gid)])
        reply_markup = telegram.InlineKeyboardMarkup(groups_keyboard)
        bot.edit_message_text(text=text, chat_id=query.message.chat_id, message_id=query.message.message_id,
                              reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)

    if ('delgrp' in query.data):
        gid = query.data.split(':')[1]
        text = "Ты уверен? Это нельзя будет отменить."
        groups_keyboard = [[InlineKeyboardButton("Да", callback_data='delgroupconf:%s' % gid)]]
        groups_keyboard.append([InlineKeyboardButton("Назад", callback_data='admgroup:%s' % gid)])
        reply_markup = telegram.InlineKeyboardMarkup(groups_keyboard)
        bot.edit_message_text(text=text, chat_id=query.message.chat_id, message_id=query.message.message_id,
                              reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)

    if ('delgroupconf' in query.data):
        gid = query.data.split(':')[1]
        text = "Ок, всё удалил."
        userlist = get_group_users(gid, ineedid=True)
        for u in userlist:
            sleep(0.1)
            bot.send_message(chat_id=u, text="Упс.\nКажется, администратор вашей группы *%s* был настоящим Гринчем.\n"
                                                 "Потому что он удалил группу.\n"
                                                 "Группы больше нет, всё пропало, Нового года не будет!\n"
                                                 "Ладно, ладно, шучу.\n"
                                                 "Всегда можно создать новую группу с помощью команды /newgroup.\n"
                                                 "И новогоднее чудо будет спасено!" % (get_group_name(gid)),
                             parse_mode=telegram.ParseMode.MARKDOWN)
        del_group(gid)
        groups_keyboard = [[]]
        reply_markup = telegram.InlineKeyboardMarkup(groups_keyboard)
        bot.edit_message_text(text=text, chat_id=query.message.chat_id, message_id=query.message.message_id,
                              reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)

    if ('delmember' in query.data):
        gid = query.data.split(':')[1]
        text = "Я сделал кнопку для каждого участника. Осторожно, дальше предупреждений не будет, участник будет " \
               "удален сразу."
        userlist = get_group_users(gid, ineedid=True)
        groups_keyboard = [[]]
        groups_keyboard.append([InlineKeyboardButton("Назад", callback_data='admgroup:%s' % gid)])
        for u in userlist:
            uname = get_user_name(u, gid)
            groups_keyboard.append([InlineKeyboardButton(uname, callback_data='delmmb:%s:%s' % (gid, u))])
        reply_markup = telegram.InlineKeyboardMarkup(groups_keyboard)
        bot.edit_message_text(text=text, chat_id=query.message.chat_id, message_id=query.message.message_id,
                              reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)

    if ('delmmb' in query.data):
        gid = query.data.split(':')[1]
        uid = query.data.split(':')[2]

        del_user_from_group(uid, gid)
        bot.send_message(chat_id=uid, text="Администратор группы *%s* удалил тебя из неё.\n" % (get_group_name(gid)),
                         parse_mode=telegram.ParseMode.MARKDOWN)

        text = "Готово! Удалим кого-нибудь еще?\n" \
               "Я сделал кнопку для каждого участника. Осторожно, дальше предупреждений не будет, участник будет " \
               "удален сразу."
        userlist = get_group_users(gid, ineedid=True)
        groups_keyboard = [[]]
        groups_keyboard.append([InlineKeyboardButton("Назад", callback_data='admgroup:%s' % gid)])
        for u in userlist:
            uname = get_user_name(u, gid)
            groups_keyboard.append([InlineKeyboardButton(uname, callback_data='delmmb:%s:%s' % (gid, u))])
        reply_markup = telegram.InlineKeyboardMarkup(groups_keyboard)
        sleep(0.1)
        bot.edit_message_text(text=text, chat_id=query.message.chat_id, message_id=query.message.message_id,
                              reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)


    if ('mygroups' in query.data):
        sh_grps(uid, bot, query=query)

    if ('admmain' in query.data):
        adm_grps(uid, bot, query=query)


def echo(bot, update):
    print("USER:", update.message.text)
    uid = update.message.from_user.id
    if uid < 0:
        return
    if (update.message.text == "300"):
        bot.send_message(chat_id=update.message.chat_id, text="Ты сам знаешь, что делать")

    elif update.message.text == "Мои группы":
        sh_grps(uid, bot, update)

        
    elif update.message.text in {"вниз", "⬇️"}:
        set_smth("screen", "down")
        stop_timer = PokaTimer(31, set_smth, "screen", "stop")
        stop_timer.start()
        jarvis.timers.append(stop_timer)
        custom_keyboard = [['⬆️'], ['⏹'], ['⬇️']]
        reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard, resize_keyboard=True)
        bot.send_message(chat_id=update.message.chat_id, text="вниз",
                         reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)

    elif update.message.text in {"вверх", "⬆️"}:
        set_smth("screen", "up")
        stop_timer = PokaTimer(31, set_smth, "screen", "stop")
        stop_timer.start()
        jarvis.timers.append(stop_timer)
        custom_keyboard = [['⬆️'], ['⏹'], ['⬇️']]
        reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard, resize_keyboard=True)
        bot.send_message(chat_id=update.message.chat_id, text="вверх",
                         reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)


    elif update.message.text in {'⏹'}:
        set_smth("screen", "stop")
        custom_keyboard = [['⬆️'], ['⏹'], ['⬇️']]
        reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard, resize_keyboard=True)
        bot.send_message(chat_id=update.message.chat_id, text="стоп",
                         reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)

    elif ("утро" in update.message.text) or ("Утро" in update.message.text):
        txt = update.message.text
        t_wake = txt.split(' ')[1]
        if len(t_wake) != 4:
            bot.send_message(chat_id=update.message.chat_id, text="Время должно состоять из 4 цифр без разделителей: 0830")
        else:
            now = datetime.datetime.now() + datetime.timedelta(hours=3) #поправка часового пояса сервера
            d_wake = datetime.datetime(now.year, now.month, now.day, int(t_wake[0:2]), int(t_wake[2:4]))
            delta_td = d_wake - now
            print(str(delta_td))
            delta = delta_td.total_seconds()
            print(str(delta))
            if delta < 0: #сперва проверяем, на сегодня ли будильник
                d_wake = d_wake + datetime.timedelta(days=1)
                delta_td = (d_wake - now)
                delta = delta_td.total_seconds()

            wake_timer = PokaTimer(delta, set_smth, "screen", "up")
            wake_stop_timer = PokaTimer(delta + 31, set_smth, "screen", "stop")#, tid=wake_timer.tid)
            jarvis.timers.append(wake_timer)
            jarvis.timers.append(wake_stop_timer)
            wake_timer.start()
            wake_stop_timer.start()
            delta_td = delta_td - datetime.timedelta(microseconds=delta_td.microseconds)
            txt = "Экран сработает через " + str(delta_td)
            kb = [[InlineKeyboardButton("Отменить", callback_data='deltimer:%s' % wake_timer.tid)]]
            reply_markup = telegram.InlineKeyboardMarkup(kb)
            bot.send_message(chat_id=update.message.chat_id, text=txt,
                             reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)




    else:
        bot.send_message(chat_id=update.message.chat_id, text="Аушки?")


if __name__ == '__main__':


    dispatcher = updater.dispatcher

    start_handler = CommandHandler("start", start, pass_args=True)
    dispatcher.add_handler(start_handler)

    trista_handler = CommandHandler("300", trista, pass_args=True)
    dispatcher.add_handler(trista_handler)

    admin_handler = CommandHandler("adm", admin)
    dispatcher.add_handler(admin_handler)

    spam_handler = CommandHandler("spam_admins", spam_admin, pass_args=True)
    dispatcher.add_handler(spam_handler)

    newgrp_handler = CommandHandler("newgroup", newgroup, pass_args=True)
    dispatcher.add_handler(newgrp_handler)

    thanks_handler = CommandHandler("thanks", thanks)
    dispatcher.add_handler(thanks_handler)

    go_handler = CommandHandler("gogogo", gogogo)
    dispatcher.add_handler(go_handler)

    echo_handler = MessageHandler(Filters.text, echo)
    dispatcher.add_handler(echo_handler)

    in_handler = CallbackQueryHandler(react_inline)
    dispatcher.add_handler(in_handler)

    updater.start_polling()



    print('start')
